package at.mnm.suncorppdf2csv;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractSuncorpStatementPdfExtractor implements SuncorpCcPdfExtractor {

   protected static final String[] month_names;
   static {
      month_names = new String[12];
      for (int i = 0; i < month_names.length; i++) {
         DateTimeFormatter monthPattern = DateTimeFormatter.ofPattern("MMM", Locale.ENGLISH);
         month_names[i] = LocalDate.parse(String.format("2020-%02d-01", i + 1)).format(monthPattern);
      }
      ;
   }

   private static DateTimeFormatter transaction_date_formatter = DateTimeFormatter
         .ofPattern("MMM dd u", Locale.ENGLISH);

   private static DateTimeFormatter range_formatter = DateTimeFormatter.ofPattern("dd/MM/yy", Locale.ENGLISH);

   private LocalDate statementRangeStart = null;

   public boolean checkCanProcessAndPrepare(String[] lines) {
      boolean foundStatementRange = false;
      boolean foundTransaction = false;
      for (String line : lines) {
         Matcher dateLineMatcher = matcherForSatementRangeOrNull(line);
         if (dateLineMatcher != null) {
            statementRangeStart = extractStatementStartDateFromDateRangeText(dateLineMatcher);
            foundStatementRange = true;
         }
         Matcher matcherForTransactionLine = matcherForTransactionLineOrNull(line);
         if (matcherForTransactionLine != null) {
            foundTransaction = true;
         }

         if (foundTransaction && foundStatementRange) {
            return true;
         }
      }
      return false;
   }

   public List<Transaction> extractTransactions(String[] lines) {
      List<Transaction> transactions = new ArrayList<>(500);
      if (getStatementRangeStart() == null) {
         throw new IllegalStateException(
               "Cannot use extractor thata hasn't successfully called checkCanProcessAndPrepare before");
      }
      for (String line : lines) {
         if (line.contains("SNEX2002") || line.contains("Langh") || line.contains("Dan Wo")  ) {
            System.out.println(line);
         }
         Matcher matcherForTransactionLine = matcherForTransactionLineOrNull(line);
         if (matcherForTransactionLine != null) {
            transactions.add(extractTransationValues(matcherForTransactionLine));
         }
      }
      return transactions;
   }

   protected LocalDate extractTransactionDateFromMatcherUsingStatementRange(Matcher matcher) {
      int year = getStatementRangeStart().getYear();
      if ("Jan".equals(matcher.group(2)) && getStatementRangeStart().getMonth().equals(Month.DECEMBER)) {
         year++;
      }
      if ("Dec".equals(matcher.group(2)) && getStatementRangeStart().getMonth().equals(Month.JANUARY)) {
         year--;
      }
      return LocalDate.parse(matcher.group(1) + " " + year, transaction_date_formatter);
   }

   protected abstract Pattern getStatmentRangeDatePattern();

   protected abstract Pattern getTransactionLinePattern();

   protected abstract Transaction extractTransationValues(Matcher matcherForTransactionLine);

   private Matcher matcherForTransactionLineOrNull(String line) {
      try {
         Matcher matcher = getTransactionLinePattern().matcher(line);
         if (matcher.matches()) {
            return matcher;
         }
      } catch (RuntimeException e) {
      }
      return null;
   }

   private LocalDate extractStatementStartDateFromDateRangeText(Matcher dateLineMatcher) {
      return LocalDate.parse(dateLineMatcher.group(1), range_formatter);
   }

   private Matcher matcherForSatementRangeOrNull(String line) {
      Matcher matcher = getStatmentRangeDatePattern().matcher(line);
      if (matcher.matches()) {
         return matcher;
      }
      return null;
   }

   public LocalDate getStatementRangeStart() {
      return statementRangeStart;
   }

}