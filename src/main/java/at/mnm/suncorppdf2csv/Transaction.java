package at.mnm.suncorppdf2csv;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Transaction {
   public final LocalDate transactionDate;
   public final String details;
   public final BigDecimal value;
   public final String reference;

   public Transaction(LocalDate transactionDate, String details, BigDecimal value, String reference) {
      this.transactionDate = transactionDate;
      this.details = details;
      this.value = value;
      this.reference = reference;
   }
}