package at.mnm.suncorppdf2csv;

import java.util.List;

public interface SuncorpCcPdfExtractor {

   List<Transaction> extractTransactions(String[] lines);

   boolean checkCanProcessAndPrepare(String[] lines);
}
