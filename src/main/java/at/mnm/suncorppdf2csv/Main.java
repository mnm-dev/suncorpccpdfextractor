package at.mnm.suncorppdf2csv;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class Main {

   private static DateTimeFormatter iso_date_formatter = DateTimeFormatter.ISO_LOCAL_DATE;

   private static SuncorpCcPdfExtractor[] extractors = new SuncorpCcPdfExtractor[] {
         new SuncorpCcPdfExtractorPre2020Feb(), new SuncorpCcPdfExtractorCurrent() };

   public static void main(String[] args) throws IOException {

      if (args.length < 1) {
         System.err.println("pass in the PDF file paths");
         System.exit(-666);
      }
      for (String fileName : args) {
         File pdfFile = new File(fileName);
         if (!pdfFile.isFile()) {
            System.err.println(pdfFile + " is not a file");
            System.exit(-667);
         }
         if (!pdfFile.canRead()) {
            System.err.println(pdfFile + " cannot be read");
            System.exit(-668);
         }
      }

      for (String fileName : args) {
         File pdfFile = new File(fileName);
         dumpCsvFromPdf(pdfFile);
      }

   }

   private static void dumpCsvFromPdf(File pdfFile) throws IOException {
      PDDocument document = PDDocument.load(pdfFile);
      List<Transaction> extractCsv = new ArrayList<>();

      if (!document.isEncrypted()) {
         PDFTextStripper stripper = new PDFTextStripper();
         String text = stripper.getText(document);
         String[] lines = text.split(stripper.getLineSeparator());

         SuncorpCcPdfExtractor extractor = findExtractor(pdfFile, lines);
         // System.out.println(text);
         extractCsv = extractor.extractTransactions(lines);
      }
      document.close();

      int nr = 1;
      for (Transaction tx : extractCsv) {
         System.out
               .println(String
                     .format("%d,%s,\"%s\",%.2f,\"%s ref:%s\"", nr++, tx.transactionDate.format(iso_date_formatter),
                           tx.details, tx.value, tx.details, tx.reference));
      }
   }

   private static SuncorpCcPdfExtractor findExtractor(File pdfFile, String[] lines) {
      for (SuncorpCcPdfExtractor extractor : extractors) {
         if (extractor.checkCanProcessAndPrepare(lines)) {
            return extractor;
         }
      }
      throw new IllegalStateException("Cannot find extractor for " + pdfFile);
   }

}
