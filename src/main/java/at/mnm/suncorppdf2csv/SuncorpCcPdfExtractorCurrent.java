package at.mnm.suncorppdf2csv;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SuncorpCcPdfExtractorCurrent extends AbstractSuncorpStatementPdfExtractor {

   private static final Pattern transaction_line_regex_pattern = Pattern
         .compile("^((" + String.join("|", month_names) + ") \\d\\d) (.*) (\\-?\\$[,\\d]+\\d.\\d\\d) [cd]r(.*)$");
   private static final Pattern statement_date_range_pattern = Pattern
         .compile("^Statement Period (\\d\\d/\\d\\d/\\d\\d) - \\d\\d/\\d\\d/\\d\\d$");

   protected Transaction extractTransationValues(Matcher matcher) {
      LocalDate transactionDate = extractTransactionDateFromMatcherUsingStatementRange(matcher);
      String details = matcher.group(3);
      BigDecimal value = BigDecimal.valueOf(Double.parseDouble(matcher.group(4).replaceAll("[\\$,]", ""))).negate();
      String reference = matcher.group(5);
      return new Transaction(transactionDate, details, value, reference);
   }

   @Override
   protected Pattern getTransactionLinePattern() {
      return transaction_line_regex_pattern;
   }

   @Override
   protected Pattern getStatmentRangeDatePattern() {
      return statement_date_range_pattern;
   }
}
